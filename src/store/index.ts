import {createStore, applyMiddleware} from 'redux'
import {reducer} from './redusers'
import {fetching} from "./middlewares/fetching";
import {fetchingTimetable} from "./middlewares/fetchingTimetable";

const enhancer = applyMiddleware(
    fetching,
    fetchingTimetable
);
export const store = createStore(reducer, enhancer);

export const fetching = (store: any) => (next: any) => (action: any) => {

  if (!action.callAPI || action.type !== 'FETCH_SHIFT') {
    return next(action)
  }

  fetch(action.callAPI, action.requestOptions)
      .then(res => res.text())
      .then(res =>{
            next({
              type: action.type,
              response: res,
            })
      }
      )
      .catch(e => {
        console.log('___---_-_!!!_-_---___', e);
      })
};

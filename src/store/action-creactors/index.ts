export const switchDayNightShift = (isDayShift: boolean) => {
  return {
    type: 'DAY_OR_NIGHT_SHIFT',
    payload: isDayShift
  }
};

export const isDayShiftReducer = (isDayShift = false, action: any) => {
  switch (action.type) {
    case 'DAY_OR_NIGHT_SHIFT':
      isDayShift = action.payload;
      return isDayShift;
    default:
      return isDayShift;
  }
};

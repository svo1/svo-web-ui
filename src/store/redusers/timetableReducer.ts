export const timetableReducer = (timeTable = [], action: any) => {
  switch (action.type) {
    case 'FETCH_TIMETABLE':
      const {entities} = action.response;
      return entities ? [...entities] : [];
    default:
      return timeTable;
  }
};

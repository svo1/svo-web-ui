import {combineReducers} from 'redux';
import {currentShiftReducer} from "./currentShiftReducer";
import {isDayShiftReducer} from "./isDayShiftReducer";
import {timetableReducer} from "./timetableReducer";

export const reducer = combineReducers ({
  currentShift: currentShiftReducer,
  isDayShift: isDayShiftReducer,
  timetable: timetableReducer
});

export const currentShiftReducer = (currentShift = '', action: any) => {
  switch (action.type) {
    case 'FETCH_SHIFT':
      currentShift = action.response;
      return currentShift;
    default:
      return currentShift;
  }
};

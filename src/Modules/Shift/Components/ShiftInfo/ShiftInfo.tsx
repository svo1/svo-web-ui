import React, {useState} from 'react';
import styles from './ShiftInfo.module.scss'
import { Typography, Space } from 'antd';

const ShiftInfo = () => {

  const { Text, Link } = Typography;

  return (
      <div className={styles.shiftInfo}>
        <Space direction="vertical">
          <Text type="success">Очистка Снега Gate 1 - ЗАДАЧА ВЫПОЛНЕНА</Text>
          <Text type="success">Очистка Снега Gate 2 - ЗАДАЧА ВЫПОЛНЕНА</Text>
          <Text type="success">Очистка Снега Gate 11 - ЗАДАЧА ВЫПОЛНЕНА</Text>
          <Text type="success">Очистка Снега Gate 15 - ЗАДАЧА ВЫПОЛНЕНА</Text>
          <Text type="warning">Очистка Снега Gate 16 - ЗАДАЧА В РАБОТЕ</Text>
          <Text type="warning">Очистка Снега Gate 10 - ЗАДАЧА В РАБОТЕ</Text>
        </Space>
      </div>
  );
};

export default ShiftInfo;

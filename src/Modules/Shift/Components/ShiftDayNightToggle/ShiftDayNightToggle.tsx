import React from 'react';
import styles from './ShiftDayNightToggle.module.scss'
import dayImg from './icons8-sun-48.png'
import nightImg from './icons8-crescent-moon-48.png'
import {connect} from "react-redux";
import {switchDayNightShift} from "../../../../store/action-creactors";

const ShiftDayNightToggle = (props: any) => {
  const {isDayShift, switchDayNightShift} = props;

  const setNightActiveClass = !isDayShift ? styles.shiftDayNightIconActive : '';
  const setDayActiveClass = isDayShift ? styles.shiftDayNightIconActive : '';

  return (
      <div className={styles.shiftDayNightToggle}>
        <img
            src={dayImg}
            alt="day"
            className={`${styles.shiftDayNightIcon} ${setDayActiveClass}`}
            // @ts-ignore
            onClick={()=>switchDayNightShift(true)}
        />
            /
        <img
            src={nightImg}
            alt="night"
            className={`${styles.shiftDayNightIcon} ${setNightActiveClass}`}
            // @ts-ignore
            onClick={()=>switchDayNightShift(false)}
        />
      </div>
  );
};

const mapStateTpProps = (state: any) => {
  return {
    isDayShift: state.isDayShift
  }
};

const mapDispatchToProps = {
  switchDayNightShift
};

export default connect(
    mapStateTpProps,
    mapDispatchToProps
)(ShiftDayNightToggle);

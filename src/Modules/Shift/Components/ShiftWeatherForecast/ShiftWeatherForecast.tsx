import React, {useState} from 'react';
import styles from './ShiftWeatherForecast.module.scss'
import ShiftWeatherForecastItem from "./ShiftHeaderWeatherForecastItem/ShiftWeatherForecastItem";
import {switchDayNightShift} from "../../../../store/action-creactors";
import {connect} from "react-redux";

export interface IShiftWeatherForecastProps {
  temperatureByTime: string[];
  weatherByTime: string[];
}

const ShiftWeatherForecast = (props: IShiftWeatherForecastProps) => {
  const {temperatureByTime, weatherByTime} = props;


  return (
      <div className={styles.shiftWeatherForecast}>
        {temperatureByTime.map((item, index) => {
          return (
              <>
                <ShiftWeatherForecastItem temperature={item} hourIndex={index} weather={weatherByTime[index]} />
              </>
          )
        })}
      </div>
  );
};

export default ShiftWeatherForecast;

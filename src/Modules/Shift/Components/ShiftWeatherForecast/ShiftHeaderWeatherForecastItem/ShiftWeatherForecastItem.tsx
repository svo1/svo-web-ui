import React from 'react';
import styles from './ShiftWeatherForecastItem.module.scss';
import weatherIconRain from '../icons8-rain-30.png';
import {connect} from "react-redux";

interface IProps {
  temperature: string;
  isDayShift: boolean;
  hourIndex: any;
  weather: any;
}
const ShiftWeatherForecastItem = (props: IProps) => {
  const {temperature, hourIndex, isDayShift, weather} = props;
  const renderTemperature = () => +temperature > 0 ? `+${temperature}` : +temperature < 0 ? `-${temperature}` : 0;
  const weatherIconSource = weather === "1" ? weatherIconRain : weather;
  const hour = isDayShift && hourIndex < 11
      ? `${hourIndex + 13}:00` : !isDayShift && hourIndex < 9
          ? `0${hourIndex}:00` : !isDayShift && hourIndex >= 10
              ? `${hourIndex}:00` : '00:00';
  return (
      <div className={styles.shiftWeatherForecastItem}>
        <span>{renderTemperature()}&#8451;</span>
        <span>{hour}</span>
      </div>
  );
};



const mapStateTpProps = (state: any) => {
  return {
    isDayShift: state.isDayShift
  }
};

export default connect(
    mapStateTpProps,
)(ShiftWeatherForecastItem);


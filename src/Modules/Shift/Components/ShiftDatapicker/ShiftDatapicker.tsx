import React, {useState, Fragment} from 'react';
import styles from './ShiftDatapicker.module.scss'
import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { KeyboardDatePicker } from "@material-ui/pickers";
import {blue} from "@material-ui/core/colors";


const ShiftDatapicker = (props: any) => {
  const [selectedDate, handleDateChange] = useState(props.currentShiftDate);

  const {setCurrentShiftDate} = props;
  const defaultMaterialTheme = createMuiTheme({
    palette: {
      primary: blue,
    },
  });

  const handleCurrentDate = (date: any) => {
    handleDateChange(date);
    setCurrentShiftDate(date);
  };

  return (
      <div className={styles.shiftHeaderDatapicker}>
        <Fragment>
          <ThemeProvider theme={defaultMaterialTheme}>
            <KeyboardDatePicker
                autoOk
                variant="inline"
                format="dd/MM/yyyy"
                value={selectedDate}
                InputAdornmentProps={{ position: "start" }}
                // @ts-ignore
                onChange={(date) => handleCurrentDate(date)}
            />
          </ThemeProvider>
        </Fragment>
      </div>
  );
};

export default ShiftDatapicker;

import React, {useEffect, useState} from 'react';
import styles from './Shift.module.scss'
import ShiftHeader from "./ShiftHeader/ShiftHeader";
import ShiftInfo from "./ShiftInfo/ShiftInfo";
import ShiftInfoDescription from "./ShiftInfoDescription/ShiftInfoDescription";
import shiftsData from '../shiftsData.json'
import {connect, useDispatch} from "react-redux";
import {FETCH_SHIFT} from "../../../store/common";
import { Button } from 'antd';
import moment from "moment";

const Shift = (props: any) => {
  const {close, selectedDate} = props;

  const [currentShiftDate, setCurrentShiftDate] = useState(selectedDate);
  const [shiftDayNight, setShiftDayNight] = useState(shiftsData[0].dayNight);
  const temperatureByTime = shiftsData[0].temperatureByTime;
  // @ts-ignore
  const weatherByTime = shiftsData[0].weatherByTime[shiftDayNight];
  const setNewDate = (date: any) => setCurrentShiftDate(date);
  const switchDayNight = (shiftDayNight: string) => setShiftDayNight(shiftDayNight);

  useEffect(() => {
    console.log('___---_-_!!!_-_---___', selectedDate);
  })
  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  const requestOptions:any = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };

  const { REACT_APP_BASE_URL } = process.env;

  const apiV1 = `${REACT_APP_BASE_URL}/v1/shifts?date=${moment(currentShiftDate).date()}-${moment(currentShiftDate).month() + 1}-${moment(currentShiftDate).year()}`

  const dispatch = useDispatch();
  const fetchSelectedShift = () => {
    dispatch({
      type: FETCH_SHIFT,
      callAPI: apiV1,
      requestOptions
    })
  };


  return (
      <div className={styles.shiftWrapper}>
        <div className={styles.shift}>
          <div className={styles.shiftHeaderWrapper}>
            <ShiftHeader
                temperatureByTime={temperatureByTime}
                setCurrentShiftDate={setNewDate}
                currentShiftDate={currentShiftDate}
                weatherByTime={weatherByTime}
            />
          </div>

          <div className={styles.shiftBodyWrapper}>
            <ShiftInfo />
            <ShiftInfoDescription />
          </div>

          <div className={styles.shiftFooter} onClick={()=>close()}>
            <Button>Закрыть</Button>
          </div>
        </div>
      </div>
  );
};

const mapStateTpProps = (state: any) => {
  return {
    currentShift: state.currentShift
  }
};

export default connect(
    mapStateTpProps,
)(Shift);

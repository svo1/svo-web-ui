import React from 'react';
import styles from './ShiftHeader.module.scss'
import ShiftDatapicker from "../ShiftDatapicker/ShiftDatapicker";
import ShiftWeatherForecast from "../ShiftWeatherForecast/ShiftWeatherForecast";
import ShiftDayNightToggle from "../ShiftDayNightToggle/ShiftDayNightToggle";

interface IProps {
  temperatureByTime: string[];
  setCurrentShiftDate: (date: any) => void;
  currentShiftDate: any;
  weatherByTime: string[];
}

const ShiftHeader = (props: IProps) => {
  const {temperatureByTime, setCurrentShiftDate, currentShiftDate, weatherByTime} = props;

  return (
      <div className={styles.shiftHeader}>
        <ShiftDatapicker currentShiftDate={currentShiftDate} setCurrentShiftDate={setCurrentShiftDate}/>
        <ShiftDayNightToggle />
        <ShiftWeatherForecast temperatureByTime={temperatureByTime} weatherByTime={weatherByTime}/>
      </div>
  );
};

export default ShiftHeader;

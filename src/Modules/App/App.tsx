import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom'
import 'antd/dist/antd.css';
import './App.css';
import AuthorizationForm from "../Authorization/Components/AuthorizationForm";
import Workspace from "../Workspace/Components/Workspace";
import Shift from "../Shift/Components/Shift";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import {store} from "../../store";
import DateFnsUtils from '@date-io/date-fns';
import {ru} from "date-fns/locale";
import {Provider} from "react-redux";
import Timetable from "../Timetable/Components/Timetable";
import Map from "../Map/Components/Map";
import WeatherForecast from "../WeatherForecast/Components/WeatherForecast";

function App() {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ru}>
      <Provider store={store}>
        <BrowserRouter>
          <Route
              path={'/'}
              component={AuthorizationForm}
              exact={true}
          />
          <Route
              path={'/timetable'}
              component={Timetable}
              exact={true}
          />
          <Route
              path={'/map'}
              component={Map}
              exact={true}
          />
          <Route
              path={'/workspace'}
              component={Workspace}
              exact={true}
          />
          <Route
              path={'/weatherforecast'}
              component={WeatherForecast}
              exact={true}
          />
        </BrowserRouter>
      </Provider>
    </MuiPickersUtilsProvider>
  );
}

export default App;

import React from 'react';
import styles from './TimetableItem.module.scss'

interface ITimetableProps {
  date: string;
  isDay: boolean;
  flightsIn: number;
  flightsOut: number;
}
const TimetableItem = (props: ITimetableProps) => {
  const {date, isDay, flightsIn, flightsOut} = props;
  return (
      <div className={styles.timeTableItem}>
        <ul>
          <li>{date}</li>
          <li>{isDay}</li>
          <li>{flightsIn}</li>
          <li>{flightsOut}</li>
        </ul>
      </div>
  );
};


export default TimetableItem;


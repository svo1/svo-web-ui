import React, {Fragment, useEffect, useState} from 'react';
import styles from './Timetable.module.scss'
import {connect, useDispatch} from "react-redux";
import {FETCH_TIMETABLE} from "../../../store/common";
import Loading from '../../../Common/Components/Loading/Loading';
import TimetableItem from "./TimetableItem/TImetableItem";
import { Calendar, Badge } from 'antd';
import ruRU from 'antd/lib/calendar/locale/ru_RU';
import moment from 'moment';
import Shift from "../../Shift/Components/Shift";
import Sidebar from "../../../Common/Components/Sidebar/Sidebar";


const Timetable = (props: any) => {
  const {timetable} = props;

  const [selectedDate, handleDateChange] = useState(new Date());
  const [isShiftOpen, toggleShiftModal] = useState(false);

  // const formatDate = () => `${selectedDate.getMonth() + 1}-${selectedDate.getFullYear()}`;

  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  const requestOptions:any = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };

  const dispatching = useDispatch();
  const fetchingTimetable = (apiV1: string) => {
    const api = `http://localhost:3000/${apiV1}`;
    dispatching({
      type: FETCH_TIMETABLE,
      callAPI: api,
      requestOptions
    })
  };



  // useEffect(() => {
  //   const apiV1 = selectedDate ? `${moment(selectedDate).month() + 1}-${moment(selectedDate).year()}` : `${moment(new Date()).month() + 1}-${moment(new Date()).year()}`;
  //   fetchingTimetable(apiV1);
  //   console.log('___---_-_!!!_-_---___', selectedDate);
  // }, [ selectedDate ]);

  const getListData = (value: any) => {
    let listData;
    listData = [
      {flightIn: timetable.find((item: any) => moment(item.date) === moment(value))?.date},
    ];
    return listData || [];
  };

  const dateCellRender = (value: any) => {
    const listData = getListData(value);
    return (
        <ul className="events">
          {listData.map((item: any) => (
              <li key={item.content}>
                {<Loading isLoaded={false}/>}
              </li>
          ))}
        </ul>
    );
  };

  const onSelect = (value: any) => {
    handleDateChange(value);
    if (value.date() === moment(selectedDate).date()) {
      return;
    }
    toggleShiftModal(true);
  };

  const onPanelChange = (value: any) => {
    handleDateChange(value);
  };


  return (

      <div className={styles.timeTableWrapper}>
        <div>
          <Sidebar />
        </div>
        <div className={styles.timeTable__items}>

          {/*{timetable.length>0 && timetable.map((item: any) => {*/}
          {/*  return (*/}
          {/*      <TimetableItem date={item.date} isDay={item.isDay} flightsIn={23} flightsOut={44}/>*/}
          {/*  )*/}
          {/*})}*/}
          {isShiftOpen ? (
              <Shift
                  shown={isShiftOpen}
                  close={() =>toggleShiftModal(false)}
                  selectedDate={moment(selectedDate)}/>
          ) : null}
        </div>
        <div className={styles.timeTable}>
          <div className={styles.timeTable__dataPicker}>
            <Calendar value={moment(selectedDate)} locale={ruRU} onChange={onSelect} onPanelChange={onPanelChange} dateCellRender={dateCellRender} />
          </div>
        </div>
      </div>

  );
};

const mapStateTpProps = (state: any) => {
  return {
    timetable: state.timetable
  }
};

export default connect(
    mapStateTpProps,
)(Timetable);

import React from 'react';
import styles from './AuthorizationForm.module.scss';
import Input from "../../../Common/Components/Input/Input";
import Button from "../../../Common/Components/Button/Button";
import Logo from "../../../Common/Components/Logo/Logo";
import {Link} from "react-router-dom";

const AuthorizationForm = () => {

  return (
      <div className={styles.authorizationFormBody}>
        <Logo />
        <Input type={"text"} title={'Учетная запись'} />
        <Input type={"password"} title={'Пароль'} />
        <Link to='/timetable'> <Button title={"Войти"} onClick={() => console.log('___---_-_!!!_-_---___', 'TEST')}/></Link>
      </div>
  );
};

export default AuthorizationForm;

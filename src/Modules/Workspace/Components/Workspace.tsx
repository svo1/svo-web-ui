import React from 'react';
import styles from './Workspace.module.scss';
import Sidebar from "../../../Common/Components/Sidebar/Sidebar";

const Workspace = () => {

  return (
      <div className={styles.workspaceBody}>
        <Sidebar />
      </div>
  );
};

export default Workspace;

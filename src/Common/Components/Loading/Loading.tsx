import React from 'react';
import {Skeleton} from "antd";

interface ILoadingProps {
  isLoaded: boolean;
}

const Button: React.FC<ILoadingProps> = ({
  isLoaded
}) => {
  return isLoaded ? null : <Skeleton active />;
};

export default Button;

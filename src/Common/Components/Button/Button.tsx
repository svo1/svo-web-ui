import React from 'react';
import styles from './Button.module.scss';

interface IButtonProps {
  title: string;
  onClick: () => void;
  isDisabled?: boolean;
}

const Button: React.FC<IButtonProps> = ({
  title,
  onClick,
 }) => {
  return (
      <button
          onClick={onClick}
          className={styles.button}
      >
        {title}
      </button>
  );
};

export default Button;

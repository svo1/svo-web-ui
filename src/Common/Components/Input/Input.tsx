import React, { useState, useRef } from 'react';
import styles from './Input.module.scss';
import InputDescription from "./InputDescription/InputDescription";

interface IInputProps {
  title: string;
  type: 'text' | 'email' | 'password' | 'tel';
  isDisabled?: boolean;
}

const Input: React.FC<IInputProps> = ({
  title,
  type,
 }) => {
  const [inputValue, setValue] = useState('');
  const inputRef = useRef(null);

  const inputHandler: any = (e: any) => {
    setValue(e.target.value);
  };

  return (
      <div style={{position: "relative"}}>
        <input
            type={type}
            ref={inputRef}
            className={styles.input}
            onChange={inputHandler}
            value={inputValue}
        />
        <InputDescription title={title} inputValue={inputValue} inputRef={inputRef} />

      </div>
  );
};

export default Input;

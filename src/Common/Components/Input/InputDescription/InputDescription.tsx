import React from 'react';
import styles from './InputDescription.module.scss';

interface IInputDescriptionProps {
  title: string;
  inputValue: string;
  inputRef: any;
}

const InputDescription: React.FC<IInputDescriptionProps> = ({
  title,
  inputValue,
  inputRef,
 }) => {
  const activeDescriptionClass = inputValue ? styles.inputDescriptionActive : '';
  return (
      <div
          className={`${styles.inputDescription} ${activeDescriptionClass}`}
          onClick={() => inputRef.current.focus()}
      >
        {title}
      </div>
  );
};

export default InputDescription;

import React, {useState} from 'react';
import styles from './Sidebar.module.scss';
import Logo from "../Logo/Logo";
import SidebarFooter from "./SidebarFooter/SidebarFooter";
import sidebarItems from  '../../../assets/sidebarItems.json'
import SidebarItem from "./SidebarItem/SidebarItem";


const Sidebar = () => {
  const [item, setItem] = useState(0);

  return (
      <div className={styles.sidebar}>
        <Logo />
        <div>
          {sidebarItems.map((sidebarItem) => {
            const test = () => setItem(sidebarItem.id);
            return (
                <div key={sidebarItem.title}>
                  <SidebarItem
                      title={sidebarItem.title}
                      itemId={sidebarItem.id}
                      selectedId={item}
                      onClickTest={test}
                      route={sidebarItem.route}
                  />
                </div>
            )
          })}
        </div>
        <SidebarFooter />
      </div>
  );
};

export default Sidebar;

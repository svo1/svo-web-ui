import React from 'react';
import styles from './SidebarFooter.module.scss';


const SidebarFooter = () => {
  return (
      <div className={styles.sidebarFooter}>
        8 (800) 999-99-99
      </div>
  );
};

export default SidebarFooter;

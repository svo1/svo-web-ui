import React from 'react';
import styles from './SidebarItem.module.scss';
import {Link} from "react-router-dom";

interface ISidebarItemProps {
  title: string;
  itemId: number;
  selectedId: number;
  onClickTest: any;
  route: string
}

const SidebarItem: React.FC<ISidebarItemProps> = ({
    title,
    itemId,
    selectedId,
    onClickTest,
    route,
}) => {
  const sidebarItemActive = itemId === selectedId ? styles.sidebarItemActive : '';

  return (
      <div className={`${styles.sidebarItem} ${sidebarItemActive}`} onClick={onClickTest}>
        <Link to={`/${route}`}>{title}</Link>
      </div>
  );
};

export default SidebarItem;

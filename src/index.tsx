import React from 'react';
import ReactDOM from 'react-dom';
import App from './Modules/App/App';
import './index.scss';
import './Common/Styles/fonts.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
